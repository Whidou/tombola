﻿#!/usr/bin/python3

# Graphical User Interface for Tombola
#
# Author: Association Éphémère <contact@association-ephemere.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from PyQt5.QtCore import QMetaObject
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import (
    QAction,
    QApplication,
    QGroupBox,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QMenu,
    QMenuBar,
    QPushButton,
    QStatusBar,
    QTreeWidget,
    QVBoxLayout,
    QWidget,
)

from os.path import abspath, join, dirname


class TombolaUI(object):
    def setupUi(self, mainWindow):
        # Window
        mainWindow.setObjectName("mainWindow")

        # Icon
        icon = QIcon()
        iconPath = abspath(join(dirname(__file__), 'Logo.ico'))
        icon.addPixmap(QPixmap(iconPath), QIcon.Normal, QIcon.Off)
        mainWindow.setWindowIcon(icon)

        # Widget container
        self.centralWidget = QWidget(mainWindow)
        self.centralWidget.setObjectName("centralWidget")
        mainLayout = QHBoxLayout(self.centralWidget)
        mainLayout.setObjectName("mainLayout")

        # Left column
        self.leftColumn = QWidget(self.centralWidget)
        self.leftColumn.setObjectName("leftColumn")
        self.leftColumn.setMinimumWidth(500)
        leftLayout = QVBoxLayout(self.leftColumn)
        leftLayout.setObjectName("leftLayout")
        mainLayout.addWidget(self.leftColumn)

        # Tickets label
        self.ticketsLbl = QLabel(self.leftColumn)
        self.ticketsLbl.setObjectName("ticketsLbl")
        leftLayout.addWidget(self.ticketsLbl)

        # Tickets list
        self.tickets = QTreeWidget(self.leftColumn)
        self.tickets.setColumnCount(3)
        self.tickets.setObjectName("tickets")
        self.tickets.header().setCascadingSectionResizes(False)
        self.tickets.header().setDefaultSectionSize(140)
        self.tickets.header().setHighlightSections(False)
        leftLayout.addWidget(self.tickets)

        # Results label
        self.resultsLbl = QLabel(self.leftColumn)
        self.resultsLbl.setObjectName("resultsLbl")
        leftLayout.addWidget(self.resultsLbl)

        # Results list
        self.results = QTreeWidget(self.leftColumn)
        self.results.setColumnCount(3)
        self.results.setObjectName("results")
        self.results.header().setDefaultSectionSize(140)
        leftLayout.addWidget(self.results)

        # Right column
        self.rightColumn = QWidget(self.centralWidget)
        self.rightColumn.setObjectName("rightColumn")
        rightLayout = QVBoxLayout(self.rightColumn)
        rightLayout.setObjectName("rightLayout")
        mainLayout.addWidget(self.rightColumn)

        # Input selector
        self.inputGroup = QGroupBox(self.rightColumn)
        self.inputGroup.setObjectName("inputGroup")
        configurationLayout = QHBoxLayout(self.inputGroup)

        # Input field
        self.inputField = QLineEdit(self.inputGroup)
        self.inputField.setObjectName("inputField")
        configurationLayout.addWidget(self.inputField)

        # Input Button
        self.inputBt = QPushButton(self.inputGroup)
        self.inputBt.setObjectName("inputBt")
        configurationLayout.addWidget(self.inputBt)

        # End input selector
        self.inputGroup.setLayout(configurationLayout)
        rightLayout.addWidget(self.inputGroup)

        # Space
        rightLayout.addStretch()

        # Draw buttons
        self.drawGroup = QGroupBox(self.rightColumn)
        self.drawGroup.setObjectName("drawGroup")
        drawLayout = QHBoxLayout(self.drawGroup)

        # Auto button
        self.autoBt = QPushButton(self.drawGroup)
        self.autoBt.setObjectName("autoBt")
        self.autoBt.setMinimumHeight(100)
        drawLayout.addWidget(self.autoBt)

        # One by one button
        self.oneBt = QPushButton(self.drawGroup)
        self.oneBt.setObjectName("oneBt")
        self.oneBt.setMinimumHeight(100)
        drawLayout.addWidget(self.oneBt)

        # End draw buttons
        self.drawGroup.setLayout(drawLayout)
        rightLayout.addWidget(self.drawGroup)

        # Save button
        self.saveBt = QPushButton(self.inputGroup)
        self.saveBt.setObjectName("saveBt")
        rightLayout.addWidget(self.saveBt)

        # Menu
        self.menubar = QMenuBar(mainWindow)
        self.menubar.setObjectName("menubar")
        self.fileMenu = QMenu(self.menubar)
        self.fileMenu.setObjectName("fileMenu")
        self.menu = QMenu(self.menubar)
        self.menu.setObjectName("menu")
        mainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(mainWindow)
        self.statusbar.setObjectName("statusbar")
        mainWindow.setStatusBar(self.statusbar)
        self.resetAction = QAction(mainWindow)
        self.resetAction.setObjectName("resetAction")
        self.quitAction = QAction(mainWindow)
        self.quitAction.setObjectName("quitAction")
        self.fileMenu.addAction(self.resetAction)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.quitAction)
        self.menubar.addAction(self.fileMenu.menuAction())
        self.menubar.addAction(self.menu.menuAction())

        # End main window
        self.centralWidget.setLayout(mainLayout)
        mainWindow.setCentralWidget(self.centralWidget)
        self.retranslateUi(mainWindow)
        QMetaObject.connectSlotsByName(mainWindow)

    def retranslateUi(self, mainWindow):
        tl = QApplication.translate

        # Main window
        mainWindow.setWindowTitle(tl("mainWindow", "Tombola"))

        # Tickets
        self.ticketsLbl.setText(tl("mainWindow", "Tickets restants"))
        self.tickets.headerItem().setText(0, tl("mainWindow", "Ticket"))
        self.tickets.headerItem().setText(1, tl("mainWindow", "Nom"))
        self.tickets.headerItem().setText(2, tl("mainWindow", "Prénom"))

        # Results
        self.resultsLbl.setText(tl("mainWindow", "Résultats"))
        self.results.headerItem().setText(0, tl("mainWindow", "Ticket"))
        self.results.headerItem().setText(1, tl("mainWindow", "Nom"))
        self.results.headerItem().setText(2, tl("mainWindow", "Prénom"))

        # Input selection
        self.inputGroup.setTitle(tl("mainWindow", "Fichier tickets"))
        self.inputBt.setText(tl("mainWindow", "Sélectionner"))

        # Save
        self.saveBt.setText(tl("mainWindow", "Sauvegarder les résultats"))

        # Draw
        self.drawGroup.setTitle(tl("mainWindow", "Tirage"))
        self.autoBt.setText(tl("mainWindow", "Tirer tout"))
        self.oneBt.setText(tl("mainWindow", "Tirer un ticket"))

        # Menus
        self.fileMenu.setTitle(tl("mainWindow", "Fichier"))
        self.resetAction.setText(tl("mainWindow", "Réinitialiser"))
        self.quitAction.setText(tl("mainWindow", "Quitter"))
        self.menu.setTitle(tl("mainWindow", "Aide"))

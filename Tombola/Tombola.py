﻿#!/usr/bin/python

# Tombola: Tirage Ordonné de Moult Babioles Ordinaires en Lots Aléatoires
#
# Author: Association Éphémère <contact@association-ephemere.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os
import sys
import random
from datetime import datetime

from PyQt5.QtWidgets import (
    QApplication,
    QFileDialog,
    QMainWindow,
    QTreeWidgetItem,
)

from .TombolaUI import TombolaUI


FILE_FILTER = "Fichiers CSV (*.csv);;Tous les fichiers (*)"
TIME_FORMAT = "%Y%m%d_%H%M%S"


class Tombola(QMainWindow):

    def __init__(self):
        """
        Main window creation and configuration
        """

        QMainWindow.__init__(self)

        self.ui = TombolaUI()
        self.ui.setupUi(self)

        self.tickets = []
        self.results = []

        # Triggers
        self.ui.inputBt.clicked.connect(self.inputSelection)
        self.ui.inputField.returnPressed.connect(self.parseInput)
        self.ui.autoBt.clicked.connect(self.drawAll)
        self.ui.oneBt.clicked.connect(self.drawOne)
        self.ui.saveBt.clicked.connect(self.export)
        self.ui.resetAction.triggered.connect(self.reset)
        self.ui.quitAction.triggered.connect(QApplication.quit)

    def reset(self, keepInput=False):
        """
        Reset the application
        """

        self.tickets = []
        self.results = []
        self.ui.tickets.clear()
        self.ui.results.clear()
        if not keepInput:
            self.ui.inputField.setText("")

    def export(self):
        """
        Export results
        """

        date = datetime.now().strftime(TIME_FORMAT)
        outputPath = "results_{}.csv".format(date)
        outputFile = open(outputPath, "w")

        for result in self.results:
            outputFile.write("{};{};{}\n".format(*result))

        outputFile.flush()
        outputFile.close()

        print("Enregistrement terminé")

    def inputSelection(self):
        """
        Input file selection
        """

        inputPath = QFileDialog.getOpenFileName(filter=FILE_FILTER)[0]
        if not inputPath:
            return

        self.ui.inputField.setText(inputPath)
        self.parseInput()

    def parseInput(self):
        """
        Input file parsing
        """

        inputPath = self.ui.inputField.text()
        if not inputPath:
            return
        if not os.path.isfile(inputPath):
            print("Fichier non trouvé")
            return

        self.reset(keepInput=True)

        data = ""
        with open(inputPath, "rb") as inputFile:
            data = inputFile.read()

        inputText = data.decode("utf-8", "replace")
        inputLines = inputText.split("\n")

        # Add tickets
        for rawTicket in inputLines:
            ticket = rawTicket.rstrip().split(';')
            if len(ticket) < 2 or (ticket[1] == ticket[2] == ""):
                continue
            self.tickets.append(ticket)

        # Update UI
        self.ui.tickets.addTopLevelItems(
            QTreeWidgetItem(t) for t in self.tickets)

    def drawOne(self):
        """
        Draw one ticket
        """

        nTickets = len(self.tickets)
        if nTickets == 0:
            print("Le tirage est terminé")
            return None

        drawnTicket = random.choice(self.tickets)
        self.results = [drawnTicket] + self.results

        remaining = []
        for ticket in self.tickets:
            if drawnTicket[1].lower() == ticket[1].lower() and \
               drawnTicket[2].lower() == ticket[2].lower():
                continue
            remaining.append(ticket)
        self.tickets = remaining

        # Update list of tickets
        self.ui.tickets.clear()
        self.ui.tickets.addTopLevelItems(
            QTreeWidgetItem(t) for t in self.tickets)

        # Update list of results
        self.ui.results.clear()
        self.ui.results.addTopLevelItems(
            QTreeWidgetItem(t) for t in self.results)
        self.ui.results.topLevelItem(0).setSelected(True)

        print("Tiré : n°{} ({} {})".format(*drawnTicket))

        return drawnTicket

    def drawAll(self):
        """
        Draw as many tickets as possible
        """

        while self.drawOne() is not None:
            pass


def main():
    """
    Main application entry point
    """

    app = QApplication(sys.argv)

    window = Tombola()
    window.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

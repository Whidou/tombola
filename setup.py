#!/usr/bin/python3

import os

from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.md')) as f:
    CHANGES = f.read()


requires = [
    'PyQt5',
]

tests_require = [
    'flake8 >= 3.8.0',
    'pytest >= 3.7.4',
    'pytest-cov',
]

standalone_require = [
    'pyinstaller',
]

setup(
    name='Tombola',
    version='2.1.0',
    description='Tombola: Tirage Ordonné de Moult Babioles Ordinaires en Lots Aléatoires',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        'Programming Language :: Python',
        'Topic :: Utilities',
    ],
    author='Association Éphémère',
    author_email='contact@association-ephemere.org',
    url='https://gitlab.com/Whidou/tombola',
    keywords='tombola random lottery utility',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    extras_require={
        'testing': tests_require,
        'standalone': standalone_require,
    },
    install_requires=requires,
    entry_points={
        'gui_scripts': [
            'Tombola = Tombola:main',
        ],
    },
)

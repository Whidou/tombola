﻿Tombola
=======

Tombola: Tirage Ordonné de Moult Babioles Ordinaires en Lots Aléatoires


Requirements
------------

* Python >= 3.6


Getting started
---------------

* Create a Python virtual environment.
```
python -m venv env
```

* Upgrade packaging tools.
```
env/*/python -m pip install --upgrade pip setuptools
```

* Install the module in editable mode with its testing requirements.
```
env/*/pip install -e ".[testing]"
```

* Run the module's tests.
```
env/*/pytest
```

* Run the application.
```
env/*/Tombola
```


Generate a stand-alone executable
---------------------------------

* Create and update a virtual environment as described in "Getting started"

* Install the module with its standalone requirements.
```
env/*/pip install ".[standalone]"
```

* Generate the executable
```
./env/*/pyinstaller -y Tombola.spec
```

* The application folder and single-file executable are generated in the
`dist` folder.
